# 邀请说明

有连接到 `Outline` 服务器的邀请吗？按照这些说明从您的设备访问开放的互联网。不会收集任何个人信息。

## 1. 复制您的访问密钥
![复制您的访问密钥](img/invitation-instructions-001.png "复制您的访问密钥")
`ss://xxx` 位于邀请电子邮件或消息的底部。`Outline` 应用程序将立即从您的剪贴板添加此服务器。


## 2. 安装 `Outline`
![安装 `Outline`](img/invitation-instructions-002.png "安装 Outline")

|下载|  |
| ------------- | ------------- |
| [`Android ›`](https://play.google.com/store/apps/details?id=org.outline.android.client) | 如果无法访问 `Google Play`，[请在此处获取](https://gitlab.com/wft44maqb/outline-releases/-/raw/master/client/Outline.apk?inline=false)。 |
| [`iOS ›`](https://itunes.apple.com/app/outline-app/id1356177741) | 在 `App Store` 上获取 `Outline` |
| [`Windows ›`](https://gitlab.com/wft44maqb/outline-releases/-/raw/master/client/Outline-Client.exe?inline=false) | 下载 `Outline.exe` 并双击启动。 |
| [`macOS ›`](https://itunes.apple.com/app/outline-app/id1356178125) | 下载`outline.dmg`，双击安装。将 `Outline` 添加到您的应用程序文件夹，双击启动。 |
| `Linux` | 请参阅下面的 `Linux` 设置说明。 |


## 3. 添加服务器并连接
![添加服务器并连接](img/invitation-instructions-003.png "添加服务器并连接")
打开应用程序并确认您的新服务器。点击或单击以连接。
**遇到麻烦？** 尝试再次复制您的访问密钥以添加您的服务器。


## `Linux` 设置说明

1. 下载 [`Shadowsocks QT5` 客户端](https://gitlab.com/wft44maqb/shadowsocks-qt5/-/raw/master/Shadowsocks-Qt5-3.0.1-x86_64.AppImage?inline=false)。
2. 将目录更改为下载的文件位置并运行：
  ```
    chmod a+x Shadowsocks-Qt5-*.AppImage && ./Shadowsocks-Qt5-*.AppImage
  ```
3. 转到`连接`->`添加`->`URI`并粘贴访问密钥 (`ss://xxx`).
4. 为连接选择一个未使用的本地端口。试试`1080`！

![Shadowsocks-QT5 端口](img/qt5-port.png "指定本地端口")

5. 右键单击新添加的连接行，然后单击`连接`。

![Shadowsocks-QT5 连接](img/qt5-connect.png "指定本地端口")

6. 转到您的浏览器和应用程序，并将它们指向您在第 5 步中选择的本地端口上的 `localhost` 上的 `SOCKS5` 代理。
